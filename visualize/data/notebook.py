import pandas as pd
import sys
from math import log, isnan
import numpy as np
import matplotlib.pyplot as plti



result = None
coinjoins = None

def stats(df):
    print(df.describe())
    print(df.head(10))

def get_tex_node(i, h, k):
    l = log(h,10)
    if isnan(l):
        l = -3
    return "(%s,0)--++(0,%s) %% %s, %s"%(i,l,log(k,10), log(k))

def output(df, which):
  #out.to_csv("result_coinjoin.csv")
  i = 1
  
  if which == "structural":
    print("\draw [red!30]")
    col = "nrCoinjoinAddresses"
  elif which == "combined":
    print("\draw [blue!30]")
    col = "nrCoinjoinAddresses"
  elif which == "subsetsum":
    print("\draw [green!30]")
    col = "nrCoinjoinAddresses"
  else: 
    col = "no_addresses"
    print("\draw [black!30]")

  for index, row in out.iterrows(): 
     print(get_tex_node(i, row[col], row["no_addresses"]))
     i += 1
     if(i>201):
       break
  print(";")

def load_out(mode, mode1):
  # "cluster" "#coinjoinTx"
  txPerCluster_df = pd.read_csv("coinjoins_txPerCluster_%s.csv"%(mode1), header=None, names=['cluster', 'nrCoinjoinTx']).set_index('cluster')
  #stats(txPerCluster_df)

  # "cluster" "#CoinjoinAddresses"
  addressesPerCluster_df = pd.read_csv("coinjoins_addrPerCluster_%s.csv"%(mode1), header=None, names=['cluster', 'nrCoinjoinAddresses']).set_index('cluster')
  #stats(addressesPerCluster_df)

  # "cluster" "#addresses"
  statsCluster_df = pd.read_csv("cluster_stats.csv").set_index('cluster')
  #stats(statsCluster_df)

  tmp = statsCluster_df.join(addressesPerCluster_df).join(txPerCluster_df)

  tmp["cj_ratio"] = tmp.apply(lambda row: row["nrCoinjoinAddresses"] / row["no_addresses"] if not pd.isna(row["nrCoinjoinAddresses"]) else 0 , axis=1)

  out =  tmp.sort_values(by=['no_addresses'], ascending=False)

  return out


def load_cj():
  return pd.read_csv("coinjoins.csv", names=["address", "tx_hash", "type", "cluster"])

def create_model():
  model = load_out("", "combined")

  df = model.query("no_addresses >= 2")

  df2 = pd.DataFrame({'mean': df.mean(), 'median': df.median(), '25%': df.quantile(0.25), '50%': df.quantile(0.5),'75%': df.quantile(0.75)})

  df2.plot()

  plti.show()

  return df



# "address", "tx_hash", "type", "cluster"
#coinjoins_df = pd.read_csv("coinjoins.csv")

# if len(sys.argv) > 1:
#   mode = sys.argv[1] # "subsetsum" "combined" "structural"
# else:
#   mode = ""


# if mode == "":
#   mode1 = "combined"
# else:
#   mode1 = mode






if __name__ == "__main__":
  #out.to_csv("result_coinjoin2.csv")
  import matplotlib.pyplot as plti

  model = load_out("", "combined")

  df = model.query("no_addresses >= 2")

  df2 = pd.DataFrame({'mean': df.mean(), 'median': df.median(),
                   '25%': df.quantile(0.25), '50%': df.quantile(0.5),
                   '75%': df.quantile(0.75)})

  df2.plot()

  plti.show()

  # print(out.describe())

  # output(out, mode)

