#!/usr/bin/env python3
import matplotlib.pyplot as plt
import pandas as pd


data = pd.read_csv('../data/txAddressCountHistogram.csv', sep=',',header=None, index_col =0)

#data = pd.read_csv('../data/weightedEdgesTxCountHistogram_sorted.csv', sep=',',header=None, index_col =0)

# data = pd.read_csv('../data/clusterEdgeCount_sorted.csv', sep=',',header=None, index_col =0)


data.plot(kind='bar')


plt.ylabel('Frequency')
plt.xlabel('Inputs')
plt.title('Histogram TX Input count Frequency')


# plt.ylabel('Frequency')
# plt.xlabel('Address tuple reuse Count')
# plt.title('Histogram Reuse same addresses / Edge weight frequency')


# plt.ylabel('edges')
# plt.xlabel('cluster')
# plt.title('Cluster/EdgeCount')



plt.show()