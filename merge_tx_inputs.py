#!/usr/bin/env python3
from collections import Counter
import sys
import os
import csv

def main():
    files = sys.stdin.readlines()

    merge_edges = Counter()

    for file_n in files:
        with open(file_n.rstrip()) as f:
            reader = csv.reader(f, delimiter=',', quotechar='"')
          
            for row in reader:
                index = row[0] + "," + row[1]
                cnt = int(row[2])
                merge_edges[index] += cnt


    counter = 0
    with open("./data/weighted_edges_parallel.csv", 'w') as edges_out:
        for c in merge_edges:
            counter += 1
            index = c
            edges_out.write("{},{}\n".format(index, str(merge_edges[c])))
            if (counter % 10000) == 0:
                print("Wrote {:,} rows to file".format(counter), end="\r")


if __name__ == "__main__":
    main()
