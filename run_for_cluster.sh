#!/bin/sh -

#145732611

set -e

cluster=./clusters/$1
cluster_id=$1

mkdir -p ${cluster} || true

# if [ ! -f "${cluster}/cluster_addresses.csv" ]; then
#     echo "get addresses for cluster"
#     ./print_cluster_addresses_by_id.py ${cluster_id} > ${cluster}/cluster_addresses.csv
# else
#     echo "addresses already loaded"
# fi

if [ ! -f "${cluster}/graph.graphml" ]; then
    echo "construct graph"
    ./create_graph.py ${cluster_id} ${cluster} ./data/weightedEdges.csv 
else
    echo "graph already loaded"
fi


if [ ! -f "${cluster}/done" ]; then
    echo "calc metrics"
    ./calc_cluster_metrics.py ${cluster} && touch done
else
    echo "Calc already done"
fi