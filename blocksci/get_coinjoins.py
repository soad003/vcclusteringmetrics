import blocksci
import matplotlib.pyplot as plt
import matplotlib.ticker
import collections
import pandas as pd
import numpy as np
%matplotlib notebook

class IterMixin(object):
    def __iter__(self):
        for attr, value in self.__dict__.items():
            yield attr, value

class CoinJoin(IterMixin):
    def __init__(self, txhash, block, typ):
        self.txhash = txhash
        self.block = block
        self.typ = typ


    def __str__(self):
        return self.name + str(self.age)

    def __hash__(self):
        return hash(self.txhash) + hash(self.typ)

     def to_dict(self):
        return {
            'txhash': self.txhash,
            'block': self.block,
            'type' : self.type
        }

# parser_data_directory should be set to the data-directory which the blocksci_parser output
chain = blocksci.Blockchain("/storage/ifishare/blksci/blocksci-bitcoin-data/")
current_block = 500000
perc_fee = 0.3
min_base_fee = 500 * 100 # chain.map_blocks(lambda block: block.net_address_type_value())
JoinMarket = blocksci.heuristics.coinjoin_txes(chain, 1, current_block) # Returns a list of all transactions in the blockchain that might be JoinMarket coinjoin transactions
(res, skipped) = blocksci.heuristics.possible_coinjoin_txes(chain, min_base_fee, perc_fee) # Returns a list of all transactions in the blockchain that might be coinjoin transactions
#blocksci.heuristics.is_coinjoin(tx: blocksci.Tx) # Uses basic structural features to quickly decide whether this transaction might be a JoinMarket coinjoin transaction
#blocksci.heuristics.is_definite_coinjoin( 
#out2 = blocksci.heuristics.possible_coinjoin_txes(chain, 1, 0.03,10)
#out2 = blocksci.heuristics.is_definite_coinjoin(chain, 100 * 500, 0.03,10)
d = set(map(lambda tx: CoinJoin(tx.hash,tx.block_height, "joinmarket"), JoinMarket))
d2 = set(map(lambda tx: CoinJoin(tx.hash,tx.block_height, "possible"), res))
u = d.union(d2)


df = pd.DataFrame.from_records([item.to_dict() for item in u])

df.to_csv("out.csv")