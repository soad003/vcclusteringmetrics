#!/usr/bin/env python3
from __future__ import print_function
import argparse
import csv
import sys
import yaml
import itertools
import os

from collections import Counter

###############################################
#
# Loads tx form csv and counts cluster merge txs
#
###############################################


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", dest="input_file",
                        default="./data/tx_inputs.csv",
                        help="Input file path")
    parser.add_argument("-o", "--output", dest="output_file",
                        default="./data/edges.csv",
                        help="Output file path")
    parser.add_argument("-e", "--errors", dest="error_file",
                        default="./data/errors.csv",
                        help="Error file path")
    args = parser.parse_args()

    print_merge_edges(args.input_file, args.output_file, args.error_file)


def print_merge_edges(input_file, output_file, error_file):
    merge_edges = Counter()
    parse_errors = 0

    try:
        os.remove(output_file)
        os.remove(error_file)
    except OSError:
        pass

    with open(error_file, 'a') as error_out:
        with open(input_file) as cluster_in:
            counter = 0
            reader = csv.reader(cluster_in, delimiter=',', quotechar='"')
          
            for row in reader:
                if not row[0].startswith("0x"):
                    continue #remove header

                counter += 1
                if (counter % 10000) == 0:
                    print("Processed {:,} rows".format(counter), end="\r")

                r = row[1]
                inputs = yaml.load(r)

                if inputs is None:
                    error_out.write(str(row) + "\n")
                    # eprint("Error", "could not parse inputs of: ", row)
                    parse_errors += 1
                    continue

                if len(inputs) >= 2:
                    addrlist = map(lambda x: x["address"].strip().lower(),
                                   inputs)
                    comb = itertools.combinations(addrlist, 2)
                    for x in comb:
                        merge_edges[x] += 1

    print("Finished edge computation. Writing {:,} edges to {}".format(
          len(merge_edges), output_file))

    counter = 0
    with open(output_file, 'a') as edges_out:
        for c in merge_edges:
            counter += 1
            a1 = str(c[0])
            a2 = str(c[1])
            edges_out.write("{},{},{}\n".format(a1, a2, str(merge_edges[c])))
            if (counter % 10000) == 0:
                print("Wrote {:,} rows to file".format(counter), end="\r")
            # print(a1 + " " + a2 + " " + str(merge_edges[c]))

    if parse_errors > 0:
        eprint("Error", "could not parse inputs of n tx: ", parse_errors)


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


if __name__ == "__main__":
    main()
