#!/bin/sh

#HDFS_PATH=hdfs://spark-master:8020/user/titanium

THRESH=100

OUTDIR=../data/cluster/all
CLUSTERFILE=../data/temp/address_clusteraa
TXFILE=../data/old/tx_inputs.csv
COINJOINS=../data/conjoins_bitcoin.csv
export SPARK_HOME="../../spark-2.2.0-bin-hadoop2.7"


echo "Cleaning up and build."
sbt clean package

echo "Executing SPARK Job"

${SPARK_HOME}/bin/spark-submit \
   --class "at.ac.uibk.ExtractionJob" \
   --master local[4] \
   --conf spark.executor.memory="10g" \
   target/scala-2.11/cluster-analysis_2.11-0.1.jar ${TXFILE} ${CLUSTERFILE} ${OUTDIR} ${COINJOINS} ${THRESH} 2>&1 | tee -a last_extraction.out

exit $?