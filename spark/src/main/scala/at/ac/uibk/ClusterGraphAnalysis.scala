package at.ac.uibk

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql._
import org.graphframes._


class ClusterGraphAnalysis(
  spark:             SparkSession,
  weightedEdgesFile: String,
  addrToIdFile: String,
  tagsFile: String,
  clusterStats: String) {

  import spark.implicits._

  // Read files

  // src, dst, txCount, blocks, cluster
  val weightedEdgesDF = spark
    .read
    .option("header", true)
    .schema(StructType(Array(
        StructField("src", StringType, true),
        StructField("dst", StringType, true),
        StructField("txCount", LongType, true),
        StructField("blocks", StringType, true),
        StructField("cluster", LongType, true)
    )))
    .csv(weightedEdgesFile)
    .toDF
    .withColumn("blocks", UDFS.parseLongArray('blocks)) 
    //.withColumn("blocks", typedLit(Seq(42L,43L,44L))) // TODO: remove once blocks are in the ds
    .persist

  // address, UID
  val addrToIdDF = spark
    .read
    .option("header", false)
    .schema(StructType(Array(
        StructField("address", StringType, true),
        StructField("UID", LongType, true))))
    .csv(addrToIdFile)
    .toDF

  // cluster, address, tag, source, actor_category, source_uri
  val tagsDF = spark
    .read
    .option("header", true)
    .schema(StructType(Array(
        StructField("cluster", LongType, true),
        StructField("address", StringType, true),
        StructField("tag", StringType, true),
        StructField("source", StringType, true),
        StructField("actor_category", StringType, false),
        StructField("description", StringType, false),
        StructField("source_uri", StringType, true),
        StructField("tag_uri", StringType, true),
        StructField("timestamp", StringType, true)
    )))
    .csv(tagsFile)
    .toDF
    .filter(s"source = 'walletexplorer.com'")
    .join(addrToIdDF, Seq("address")).cache

  // cluster, no_addresses (nodes in cluster)
  val clusterStatsDF = spark
    .read
    .option("header", true)
    .schema(StructType(Array(
        StructField("cluster", LongType, true),
        StructField("no_addresses", LongType, true))))
    .csv(clusterStats)
    .toDF.cache

  // Edge statistics

  // txCount, freq (edge weight distr)
  val weightedEdgesHistDF = weightedEdgesDF
    .groupBy('txCount)
    .agg(count('*) as "freq")
    .select('txCount, 'freq)

  // cluster, count (edges in cluster)
  val clusterEdgeCountDF = weightedEdgesDF
    .groupBy('cluster)
    .agg(count('*) as "count")
    .select('cluster, 'count)

  // count, freq (edge count dist)
  val clusterEdgeCountHistDF = clusterEdgeCountDF
    .groupBy('count)
    .agg(count('*) as "freq")
    .select('count, 'freq)  
}