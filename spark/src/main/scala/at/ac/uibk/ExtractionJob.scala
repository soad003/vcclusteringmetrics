
package at.ac.uibk

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.SparkSession

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{ FileSystem, Path }

object ExtractionJob {

  def main(args: Array[String]): Unit = {

    val spark = SparkSession
      .builder
      .appName("Cluster Graph Dataset Extraction")
      .getOrCreate()
    spark.sparkContext.setLogLevel("WARN")

    args.foreach(println(_))

    val txFile = args(0)
    val addrClusterFile = args(1)
    val outputPath = args(2)
    val coinjoinPath = args(3)
    val txThresh = args(4).toLong

    val fs = {
      val conf = new Configuration()
      conf.set("fs.defaultFS", outputPath)
      FileSystem.get(conf)
    }

    def writeAsString(hdfsPath: String, content: String) {
      val path: Path = new Path(hdfsPath)
      if (fs.exists(path)) {
        fs.delete(path, true)
      }
      val os = fs.create(path)
      os.write(content.getBytes)
      os.close()
    }

    def printColumns(df: DataFrame) = {
      df.columns.reduce((s1, s2) => s1 + "," + s2)
    }

    def save(df: DataFrame, filename: String) = {
      df
        .write
        .mode(SaveMode.Overwrite)
        .option("header", false)
        .csv(outputPath + '/' + filename)

      writeAsString(
        outputPath +
          '/' +
          filename.split("\\.").head + "_headers." + filename.split("\\.").last,
        printColumns(df))

    }

    val dataset = new ClusterGraphDataset(spark, txFile, addrClusterFile, coinjoinPath, txThresh)

    println("Create Integer Ids for Addresses")
    save(dataset.addrToLongDF, "addrToId.csv")

    println(s"Loaded and cached ${dataset.addressClusterDF.count} address cluster mappings...")

    println("Display tx / input address summary statistics")
    dataset.txInputsDF.describe("addressCount").show

    println("Saving transaction address count histogram  (bin 1)")
    save(dataset.txAddressCountHistDF, "txAddressCountHistogram.csv")

    println("Display tx / input address & combination count summary statistics")
    dataset.addrCombinationsDF.describe("addressCount", "combinationsCount").show

    println("Save weighted edges")
    save(dataset.weightedEdgesDF, "weightedEdges.csv")

    println("Save txs no included because auf threshold")
    save(dataset.txNotIncluded, "txs_notIncluded.csv")

    spark.stop()
  }

}


