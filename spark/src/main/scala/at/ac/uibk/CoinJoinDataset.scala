package at.ac.uibk

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql._

class CoinJoinDataset(
  spark:           SparkSession,
  txFile:          String,
  addrClusterFile: String,
  coinjoinsFile: String) {

  import spark.implicits._

  // Read files

  // array(address, value), height
  val txInputsRawDF = spark
    .read
    .option("header", true)
    .csv(txFile)
    .toDF

  // Clean inputs
  val schemaInput = ArrayType(
    new StructType()
      .add("address", StringType, false)
      .add("value", LongType, false))

  val txInputsCleanRawDF = txInputsRawDF
    .filter('inputs.isNotNull)  // remove coinbase
    .withColumn("inputs", from_json(UDFS.udf_sanitizeJSON('inputs), schemaInput))
    

  // address, cluster
  val addressClusterDF = spark
    .read
    .option("header", true)
    .csv(addrClusterFile)
    .toDF
    .cache

  // nr, block, txhash, type (subsetsum, structural)
  val potentialCoinjoins = spark
    .read
    .option("header", true)
    .csv(coinjoinsFile)
    .toDF
    .select('block as "height", 'txhash as 'tx_hash, 'type)

  val addressesInvolvedInCJ = txInputsCleanRawDF
    .select($"inputs.address" as "addresses", 'tx_hash) // remove values from inputs  
    .join(potentialCoinjoins,  Seq("tx_hash"))
    .withColumn("address", explode('addresses))
    .select("address", "tx_hash", "type")
    .join(addressClusterDF, Seq("address"))
    .select("address", "tx_hash", "type", "cluster")

  val addressesInvolvedInCJ_structural = addressesInvolvedInCJ.filter("type == 'structural'")
  val addressesInvolvedInCJ_subsetsum = addressesInvolvedInCJ.filter("type == 'subsetsum'")

  val coinjoinTxsPerCluster_structural = addressesInvolvedInCJ_structural
    .select('tx_hash, 'cluster)
    .distinct
    .groupBy('cluster)
    .agg(count('*) as "freq")

  val coinjoinTxsPerCluster_subsetsum = addressesInvolvedInCJ_subsetsum
    .select('tx_hash, 'cluster)
    .distinct
    .groupBy('cluster)
    .agg(count('*) as "freq")

  val coinjoinAddrPerCluster_structural = addressesInvolvedInCJ_structural
    .select('address, 'cluster)
    .distinct
    .groupBy('cluster)
    .agg(count('*) as "freq")

  val coinjoinAddrPerCluster_subsetsum = addressesInvolvedInCJ_subsetsum
    .select('address, 'cluster)
    .distinct
    .groupBy('cluster)
    .agg(count('*) as "freq")

}