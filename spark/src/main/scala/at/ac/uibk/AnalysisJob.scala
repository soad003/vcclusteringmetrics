package at.ac.uibk

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql.Row
import org.graphframes._
import org.apache.spark.sql.types._

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{ FileSystem, Path }

import scala.collection.mutable.WrappedArray

import org.graphframes._

object Session {
  val spark = SparkSession
      .builder
      .appName("Cluster Graph Dataset Analysis")
      .getOrCreate()
}

object GraphAnalysis extends Serializable {
  import Session.spark.implicits._

  def printDFPart(df: DataFrame, label: String) : Unit = { 
    val c = df.count
    println(label)
    println(s"count: $c : $df") 
    df.show(20, false)
    df.printSchema
  }
  def printDFPart(df: GraphFrame, label: String) : Unit = { 
    printDFPart(df.edges, label)
    printDFPart(df.vertices, label)
  }

  def createGraph(df: DataFrame, cluster: Long) : GraphFrame = {
    //println("Filter edges of cluster")
    val e = df.filter(s"cluster = $cluster") // needs src and dst column
    //println("Calc verticies from cluster")
    val v = e.select('src as "id")
          .union(e.select('dst as "id"))
          .distinct

    GraphFrame(v, e)
  }

  def analyseCluster(weightedEdgesDF: DataFrame, tags: DataFrame, cluster_id: Long) = {
    println(s"Analyse cluster $cluster_id")
    val g = createGraph(weightedEdgesDF, cluster_id)

    println("Decay Analysis")
    val tree = analyseClusterDecay(g, cluster_id).toString

    println("Find conflicts Analysis")
    val tagConflicts = analyseConflictingTags(g, tags ,cluster_id).toString


    // println("waiting for input")
    // scala.io.StdIn.readLine()
    (tree, tagConflicts)
  }

  def analyseClusterDecay(g: GraphFrame, cluster_id: Long) : Option[DecayTree] = {
    import Session.spark.implicits._

    // collect blocks in which trans edges were created, add to prio queue
    val relevantBlocks = g.edges.agg(collect_set("blocks") as "blocks").first.getAs[WrappedArray[WrappedArray[Long]]]("blocks")
   

    if(relevantBlocks.length > 0){
      val decaytree = decay(g, cluster_id, relevantBlocks(0))
      
      return Some(decaytree)

    } else return None
   
    // while blocks are there fetch most recent
    // find edges created at this block 
    // decrement edge weight, remove edge if zero weight is reachd
    // if edge remove check for connected components. more than one? check size
  }

  abstract class DecayTree
  case class Branch(children: List[DecayTree], vertices:Long, decayBlock: Long) extends DecayTree
  case class Leaf(vertices:Long) extends DecayTree

  def decay(g: GraphFrame, cluster_id: Long, relevantBlocks: Seq[Long]) : DecayTree = {
    import Session.spark.implicits._
    
    println(relevantBlocks)
    if(relevantBlocks.length < 1) return Leaf(g.vertices.count)


    // START REMOVE EDGES OF THIS BLOCK
    val maxBlock = relevantBlocks.max
    val vertexCount = g.vertices.count
    val relevantEdges = g.edges.filter(row => row.getAs("blocks").asInstanceOf[WrappedArray[Long]].contains(maxBlock))

/*    printDFPart(relevantEdges)
    printDFPart(g.edges)*/

    // src, dst, txCount, blocks, cluster
    val edges_new = g.edges
                      .withColumn("maxBlock", typedLit(maxBlock))
                      .withColumn("newBlocks", UDFS.rmMaxBlock('blocks, 'maxBlock))
                      .withColumn("newTxCount", UDFS.decTxCount('blocks, 'maxBlock, 'txCount))
                      .select('src, 'dst, 'newTxCount as "txCount", 'newBlocks as "blocks", 'cluster)
                      .filter("txCount > 0").toDF

    //printDFPart(edges_new, "NEW EDGES FOR NEXT ROUND")

    val gprime = createGraph(edges_new, cluster_id)

    //printDFPart(gprime, "NEW EDGES FOR NEXT ROUND")

    // FIND COMPOMENTS
    println("CALC CONNECTED COMPONENTS")
    val cc = gprime.connectedComponents.run()

    // only need to join src because dest must be in same cc
    val gprimeCC = gprime.edges.join(cc, gprime.edges.col("src") === cc.col("id")) 

    val componentsList = cc.groupBy("component").count().collect().map(r => (r(0).asInstanceOf[Long], r(1).asInstanceOf[Long])).toList

    // RECURSIVE CALL FOR SUB COMPONENTS

    println("REC")

    val subCC = componentsList.map((t) =>{
      val(component, cnt) = t
      println("create sub graph")
      val gsub = createGraph(gprimeCC.filter(s"component = $component"), cluster_id)
      println("created sub graph")
      decay(gsub, cluster_id, relevantBlocks.filterNot(_ == maxBlock))
    })

    if(subCC.length > 0) Branch(subCC, vertexCount, maxBlock)
    else Leaf(vertexCount)
  }

  def analyseConflictingTags(g: GraphFrame, tags: DataFrame, cluster_id: Long) = {
    import Session.spark.implicits._
    // find tags for cluster
    // find tags on edges that are conflictinc
    // search for categories where we have confilcts, Define conflicts

    /*printDFPart(tags)*/

    tags.groupBy("actor_category").count.collect().map(r => (r(0).asInstanceOf[String], r(1).asInstanceOf[Long])).toList
  }

}

object AnalysisJob {

  val DEBUG = true

  def main(args: Array[String]): Unit = {
    import Session.spark.implicits._

    //  ${EDGES_FILE} ${OUTDIR} ${ADR_TO_ID_FILE} ${TAGS_FILE} ${STATS_FILE}
    val weightedEdgeFile = args(0)
    val outputPath = args(1)
    val addrToIdPath = args(2)
    val tagsPath = args(3)
    val clusterStats = args(4)
    val from_cluster_size = args(5).toLong
    val to_cluster_size = args(6).toLong
    val checkpointDir = args(7)

    Session.spark.sparkContext.setLogLevel("OFF")
    Session.spark.sparkContext.setCheckpointDir(checkpointDir) // https://jaceklaskowski.gitbooks.io/mastering-apache-spark/spark-rdd-checkpointing.html

    args.foreach(println(_))

 /*   val fs = {
      val conf = new Configuration()
      conf.set("fs.defaultFS", outputPath)
      FileSystem.get(conf)
    }

    def writeAsString(hdfsPath: String, content: String) {
      val path: Path = new Path(hdfsPath)
      if (fs.exists(path)) {
        fs.delete(path, true)
      }
      val os = fs.create(path)
      os.write(content.getBytes)
      os.close()
    }*/

    def save(df: DataFrame, outputPath: String, filename: String) = {
      df
        .write
        .mode(SaveMode.Overwrite)
        .option("header", false)
        .csv(outputPath + '/' + filename)
/*
      if (!DEBUG)
        writeAsString(
          outputPath +
            '/' +
            filename.split("\\.").head + "_headers." + filename.split("\\.").last,
          printColumns(df))*/

    }

    val dataset = new ClusterGraphAnalysis(Session.spark, weightedEdgeFile, addrToIdPath, tagsPath, clusterStats)
    
    println(s"SPARK LOCAL : $Session.spark.isLocal")
/*
    println("Display edge / transaction count summary statistics")
    dataset.weightedEdgesDF.describe("txCount").show

    println("Save weighted edges histogram  (bin 1)")
    save(dataset.weightedEdgesHistDF, outputPath, "weightedEdgesTxCountHistogram.csv")

    println("Save number of edges per cluster")*/
    save(dataset.clusterEdgeCountDF, outputPath, "clusterEdgeCount.csv")

    // println("Save cluster edge count histogram (bin 1)")


    dataset.tagsDF.show(10, false)
    dataset.addrToIdDF.show(10, false)
    dataset.clusterStatsDF.show(10, false)


    // ANALYSIS PART

    //val t = dataset.clusterStatsDF.filter(s"no_addresses > $from_cluster_size AND no_addresses < $to_cluster_size") // only cluster greater than a certain threshold
    val t = dataset.clusterStatsDF.filter(s"cluster =  69197745") // only cluster greater than a certain threshold

    
    val tc = t.count

    println(s"Cluster in Range $from_cluster_size - $to_cluster_size: Containing $tc cluster")
/*    printDFPart(t)*/

    println("Entering cluster Analysis LOOP")

    val res = t.select("cluster").rdd.map(r => r(0)).collect().map((row) => { 
      val cluster_id = row.asInstanceOf[Long] //("cluster")

      val bla = dataset.weightedEdgesDF.filter(s"cluster = $cluster_id")

      bla.show()
 
      GraphAnalysis.analyseCluster(bla, dataset.tagsDF.filter(s"cluster = $cluster_id"), cluster_id) 
    })

    println("Saving output")
    save(Session.spark.sparkContext.parallelize(res).toDF(), outputPath, "cluster_analysis.csv")

    Session.spark.stop()
  }

  def printColumns(df: DataFrame) = {
    df.columns.reduce((s1, s2) => s1 + "," + s2)
  }
  
}


