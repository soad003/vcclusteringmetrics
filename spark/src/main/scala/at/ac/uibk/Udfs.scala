package at.ac.uibk

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql._

object UDFS extends Serializable {

  def sanitizeJSON(s: String): String =
    s match {
      case x: String => x.replaceAll("'", """"""")
        .replaceAll("address", """"address"""")
        .replaceAll("value", """"value"""")
      case _ => null
    }

  val udf_sanitizeJSON = udf[String, String](sanitizeJSON)

  val addrCombinationsUDF = udf((addresses: Seq[Long]) => {
    addresses.distinct.sorted.combinations(2).toArray
  })

  val decTxCount = udf((blocks: Seq[Long], max: Long, txCount: Long) => {
    if(blocks.contains(max)) txCount - 1
    else txCount
  })

  val rmMaxBlock = udf((blocks: Seq[Long], max: Long) => {
    blocks.filterNot((x) => x == max)
  })

  val stringify = udf((vs: Seq[String]) => vs match {
    case null => null
    case _    => s"""[${vs.mkString(",")}]"""
  })
/*
  val deStringyfy = udf((s: string) => s match {
    case null => null
    case c => 
  })*/

  val parseLongArray = udf((blocks: String) => {
    blocks.drop(1).dropRight(1).split(",").map(_.toLong)
  })

}