package at.ac.uibk

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql._

class ClusterGraphDataset(
  spark:           SparkSession,
  txFile:          String,
  addrClusterFile: String,
  coinjoinsFile: String,
  tx_thrsh: Long) {

  import spark.implicits._

  // Read files

  // array(address, value), height
  val txInputsRawDF = spark
    .read
    .option("header", true)
    .csv(txFile)
    .toDF

  // Clean inputs
  val schemaInput = ArrayType(
    new StructType()
      .add("address", StringType, false)
      .add("value", LongType, false))

  val txInputsCleanRawDF = txInputsRawDF
    .filter('inputs.isNotNull)  // remove coinbase
    .withColumn("inputs", from_json(UDFS.udf_sanitizeJSON('inputs), schemaInput))
    

  // address, cluster
  val addressClusterDF = spark
    .read
    .option("header", true)
    .csv(addrClusterFile)
    .toDF
    .cache

  // nr, block, txhash, type (subsetsum, structural)
  val potentialCoinjoins = spark
    .read
    .option("header", true)
    .csv(coinjoinsFile)
    .toDF
    .select('block as "height", 'txhash as 'tx_hash, 'type)

  val addressesInvolvedInCJ = txInputsCleanRawDF
    .select($"inputs.address" as "addresses", 'tx_hash) // remove values from inputs  
    .join(potentialCoinjoins,  Seq("tx_hash"))
    .withColumn("address", explode('addresses))
    .select("address", "tx_hash", "type")
    .join(addressClusterDF, Seq("address"))
    .select("address", "tx_hash", "type", "cluster")

  val coinjoinTxsPerCluster = addressesInvolvedInCJ
    .select('tx_hash, 'cluster)
    .distinct
    .groupBy('cluster)
    .agg(count('*) as "freq")

  val coinjoinAddrPerCluster = addressesInvolvedInCJ
    .select('address, 'cluster)
    .distinct
    .groupBy('cluster)
    .agg(count('*) as "freq")


  // Computation of Integer Addresses 

  // address, UID
  val addrToLongDF = addressClusterDF
                     .select('address, 'cluster)
                     .distinct
                     .withColumn("UID", monotonically_increasing_id).cache



  // array(address), height, addressCount
  val txInputsDF = txInputsCleanRawDF
    .select($"inputs.address" as "addresses", 'height) // remove values from inputs  
    .withColumn("txId", monotonically_increasing_id)
    .withColumn("address", explode('addresses))
    .join(addrToLongDF,  Seq("address"))
    .groupBy('txId).agg(collect_list('UID) as "addresses", first('height) as "height")
    .withColumn("addressCount", size('addresses))
    .filter('addressCount > 1)
    .select('addresses, 'height, 'addressCount)
    .cache

  // addressCount, freq (frequency of specific nr of inputs)
  val txAddressCountHistDF = txInputsDF
    .groupBy('addressCount)
    .agg(count('*) as "freq")
    .select('addressCount, 'freq)

    
  // Computation of input address combinations
/*  val tx_thrsh = 100*/

  // tx_hash, array(address), block, addressCount, array((address, address)), combinationsCount
  val addrCombinationsDF = txInputsDF
    .filter('addressCount < tx_thrsh) // added threshold in order to avoid mem explosion
    .repartition(1000)
    .withColumn("combinations", UDFS.addrCombinationsUDF('addresses))
    .withColumn("combinationsCount", size('combinations))

  val txNotIncluded = txInputsDF.filter('addressCount >= tx_thrsh).withColumn("addresses", UDFS.stringify($"addresses"))

  // Computation of edges

  // src, dst, height
  val edgesDF = addrCombinationsDF
    .select('combinations, 'height)
    .withColumn("edge", explode('combinations))
    .select(
      'edge.getItem(0) as "src",
      'edge.getItem(1) as "dst",
      'height)

  // src, dst, block, cluster
  val weightedEdgesDF = edgesDF
    .groupBy('src, 'dst)
    .agg(count('*) as "txCount", 
      collect_list("height") as "blocks")
    .select('src, 'dst, 'txCount, 'blocks)
    .withColumn("blocks", UDFS.stringify($"blocks"))
    .join(addrToLongDF.select('UID as "src", 'cluster), Seq("src")) // "left_outer"
/*    .join(addrToLongDF.select('address as "src", 'UID as "UID_SRC"), Seq("src"), "left_outer")
    .join(addrToLongDF.select('address as "dst", 'UID as "UID_DST"), Seq("dst"), "left_outer")*/
    .select('src, 'dst ,'txCount, 'blocks, 'cluster)

}