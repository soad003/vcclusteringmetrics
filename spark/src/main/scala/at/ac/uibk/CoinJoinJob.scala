
package at.ac.uibk

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.SparkSession

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{ FileSystem, Path }

object CoinJoinJob {

  def main(args: Array[String]): Unit = {

    val spark = SparkSession
      .builder
      .appName("Cluster Graph Dataset Extraction")
      .getOrCreate()
    spark.sparkContext.setLogLevel("WARN")

    args.foreach(println(_))

    val txFile = args(0)
    val addrClusterFile = args(1)
    val outputPath = args(2)
    val coinjoinPath = args(3)

/*    val fs = {
      val conf = new Configuration()
      conf.set("fs.defaultFS", outputPath)
      FileSystem.get(conf)
    }
*/
    def writeAsString(hdfsPath: String, content: String) {
/*      val path: Path = new Path(hdfsPath)
      if (fs.exists(path)) {
        fs.delete(path, true)
      }
      val os = fs.create(path)*/
/*      os.write(content.getBytes)
      os.close()*/
    }

    def printColumns(df: DataFrame) = {
      df.columns.reduce((s1, s2) => s1 + "," + s2)
    }

    def save(df: DataFrame, filename: String) = {
      df
        .write
        .mode(SaveMode.Overwrite)
        .option("header", false)
        .csv(outputPath + '/' + filename)


    }

    val dataset = new CoinJoinDataset(spark, txFile, addrClusterFile, coinjoinPath)

    println("Coincoin Dataset")
    save(dataset.addressesInvolvedInCJ, "coinjoins.csv")

    println("Coincoin Txs per cluster, structural")
    save(dataset.coinjoinTxsPerCluster_structural, "coinjoins_txPerCluster_structural.csv")

    println("Coincoin addr per cluster structural")
    save(dataset.coinjoinAddrPerCluster_structural, "coinjoins_addrPerCluster_structural.csv")

    println("Coincoin Txs per cluster, subsetsum")
    save(dataset.coinjoinTxsPerCluster_subsetsum, "coinjoins_txPerCluster_subsetsum.csv")

    println("Coincoin addr per cluster subsetsum")
    save(dataset.coinjoinAddrPerCluster_subsetsum, "coinjoins_addrPerCluster_subsetsum.csv")
  

    spark.stop()
  }

}


