# Notes
Env variable `$SPARK_HOME` has to be set. Currently for dev purposes it is hardcoded in `execute.sh`. If job runs out of disk space, config("spark.local.dir", "") can be set to a dir with sufficient disk space, std /tmp/spark... 

The spark job takes as inputs 2 files the clusters and the tx csv file (first 2 paramters) and as third parameter it spezifies the output folder for the edge csv.

Values are set in `execute.sh`

`OUTDIR=../data/cluster/all`

`CLUSTERFILE=../data/temp/address_clusteraa`

`TXFILE=../data/tx_inputs.csv`

`export SPARK_HOME="../../spark-2.2.0-bin-hadoop2.7"`



