#!/bin/sh

HDFS_PATH=hdfs://spark-master:8020/user/titanium


TXFILE=${HDFS_PATH}/tx_inputs_hash.csv
CLUSTERFILE=${HDFS_PATH}/address_cluster.csv

COINJOINS=${HDFS_PATH}/coinjoins_bitcoin.csv

OUTDIR=${HDFS_PATH}/dataset${THRESH}


echo "Cleaning up and build."
sbt clean package

echo "Executing SPARK Job"

$SPARK_HOME/bin/spark-submit \
     --class "at.ac.uibk.CoinJoinJob" \
     --master "spark://spark-master:7077" \
     --conf spark.executor.memory="220g" \
     target/scala-2.11/cluster-analysis_2.11-0.1.jar ${TXFILE} ${CLUSTERFILE} ${OUTDIR} ${COINJOINS} 2>&1 | tee -a last_coinjoin.out

exit $?