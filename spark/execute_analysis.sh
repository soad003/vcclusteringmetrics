#!/bin/sh

DATASET_DIR=dataset200

HDFS_PATH=hdfs://spark-master:8020/user/titanium

EDGES_FILE=${HDFS_PATH}/${DATASET_DIR}/weightedEdges.csv

TAGS_FILE=${HDFS_PATH}/cluster_tags.csv

ADR_TO_ID_FILE=${HDFS_PATH}/${DATASET_DIR}/addrToId.csv

STATS_FILE=${HDFS_PATH}/cluster_stats.csv

FROM_CLUSTER_SIZE=100

TO_CLUSTER_SIZE=1000

CHECKPOINT_DIR=hdfs://spark-master:8020/user/titanium/temp/checkpoints

OUTDIR=${HDFS_PATH}/datasetAnalysis


echo "Cleaning up and build."
sbt clean package

echo "Executing SPARK Job"

$SPARK_HOME/bin/spark-submit \
    --class "at.ac.uibk.AnalysisJob" \
    --master "spark://spark-master:7077" \
    --conf spark.executor.memory="220g" \
    --packages graphframes:graphframes:0.5.0-spark2.1-s_2.11 \
    target/scala-2.11/cluster-analysis_2.11-0.1.jar ${EDGES_FILE} ${OUTDIR} ${ADR_TO_ID_FILE} ${TAGS_FILE} ${STATS_FILE} ${FROM_CLUSTER_SIZE} ${TO_CLUSTER_SIZE} ${CHECKPOINT_DIR} 2>&1 | tee -a last_graph_analysis.out

exit $?