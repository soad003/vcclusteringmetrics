now=$(date +"%m_%d_%Y")

DATASET_PATH=../../data/aggregated

filename=$DATASET_PATH/cluster_analytics_dataset_$now.tar.bz2

if [ -f $filename ]
then
    echo "$filename already exists"
else
    echo "Packing dump files into $filename"
    tar cf $filename --use-compress-prog=pbzip2 $DATASET_PATH/*.csv
fi

gsutil cp $filename gs://graphsense-research/cluster-analysis/
