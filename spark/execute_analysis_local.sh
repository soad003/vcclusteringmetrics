#!/bin/sh

EDGES_FILE=../data/wE69197745.csv
TAGS_FILE=../data/cluster_tags_69197745.csv
OUTDIR=../data/temp/analysis
ADR_TO_ID_FILE="../data/cluster/all/addrToId/addrToId.csv"
STATS_FILE=../data/cluster_stats.csv

FROM_CLUSTER_SIZE=100

TO_CLUSTER_SIZE=1000

CHECKPOINT_DIR=/home/mfrowis/temp/spark_temp/checkpoints

export SPARK_HOME="../../spark-2.2.0-bin-hadoop2.7"


echo "Cleaning up and build."
sbt clean package

echo "Executing SPARK Job"

# $SPARK_HOME/bin/spark-submit \
#     --class "at.ac.uibk.AnalysisJob" \
#     --master "spark://spark-master:7077" \
#     --conf spark.executor.memory="10g" \
#     --packages graphframes:graphframes:0.5.0-spark2.1-s_2.11
#     target/scala-2.11/cluster-analysis_2.11-0.1.jar ${EDGES_FILE} ${OUTDIR} ${ADR_TO_ID_FILE} ${TAGS_FILE} ${STATS_FILE}


${SPARK_HOME}/bin/spark-submit \
    --class "at.ac.uibk.AnalysisJob" \
    --master local[4] \
    --conf spark.executor.memory="6G" \
    --conf spark.driver.memory="12G" \
    --conf spark.local.dir=/home/mfrowis/temp/spark_temp \
    --conf org.apache.spark.SparkContext.setCheckpointDir=/home/mfrowis/temp/spark_temp/checkpoints \
    --packages graphframes:graphframes:0.5.0-spark2.1-s_2.11 \
    target/scala-2.11/cluster-analysis_2.11-0.1.jar ${EDGES_FILE} ${OUTDIR} ${ADR_TO_ID_FILE} ${TAGS_FILE} ${STATS_FILE} ${FROM_CLUSTER_SIZE} ${TO_CLUSTER_SIZE} ${CHECKPOINT_DIR} 


exit $?