#!/bin/bash

PARAM1=$1

HDFS_PATH=hdfs://spark-master:8020/user/titanium/dataset${PARAM1}

TARGET_PATH=../../data/aggregated


for file in `hadoop fs -ls $HDFS_PATH | sed '1d;s/  */ /g' | cut -d\  -f8`
do
    filename=`basename $file`
    echo "Copy and merge $filename from HDFS to local..."
    hadoop fs -getmerge $file $TARGET_PATH/$filename
done

rm $TARGET_PATH/.*.crc