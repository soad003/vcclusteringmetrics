#!/usr/bin/env python
import graph_tool.all as gt
import csv
import os
import matplotlib
import sys

# 1.) Ein ungerichteter Graph in dem Knoten Adressen und Kanten die Zahl der Transaktionen in denen zwei Adressen gemeinsam vorgekommen sind. Beispiel:

# a1 a2 3
# a2 a4 2
# a5 a6 1

# 2.) Die Zuordnung der Adressen zu Clustern (berechnet mittels multiple-input Heuristik). Beispiel:

# a1 c1
# a2 c1
# a4 c1
# a5 c2
# a6 c2


g = gt.Graph(directed=False)
VP_NAME = "name"
EP_WEIGHT = "weight"
GP_NTOV = "name_to_vertex"

g.edge_properties[EP_WEIGHT] = g.new_edge_property("int")
g.vertex_properties[VP_NAME] = g.new_vertex_property("string")
# g.vertex_properties["pagerank"] = g.new_vertex_property("double")

# g.vertex_properties["betwenness"] = g.new_vertex_property("double")
# g.edge_properties["betwenness"] = g.new_edge_property("double")

# g.vertex_properties["closeness"] = g.new_vertex_property("double")

# g.graph_properties["central_point_dominance"] = g.new_graph_property("double")

# g.graph_properties["eigenvalue"] = g.new_graph_property("double")
# g.vertex_properties["eigenvector"] = g.new_vertex_property("double")

# g.vertex_properties["katz"] = g.new_vertex_property("double")

# g.vertex_properties["hits_x"] = g.new_vertex_property("double")
# g.vertex_properties["hits_y"] = g.new_vertex_property("double")
# g.graph_properties["hits_eigenvalue"] = g.new_graph_property("double")

# g.vertex_properties["mincutparts"] = g.new_vertex_property("boolean")
# g.graph_properties["mincutvalue"] = g.new_graph_property("double")

#g.graph_properties[GP_NTOV] = g.new_graph_property("python::object",{})
ntov = {}

def main(cluster_to_analyse, outPath, edge_file):
    load_cluster(cluster_to_analyse, edge_file)  

    save_cluster_graph_to_file(outPath)

def load_cluster(cluster_to_analyse, edge_file):
    # if os.path.isdir(cluster_to_analyse):
    #     load_cluster_graph_from_file(cluster_to_analyse)
    # else:
    # with open(cluster_file) as file:  
    #     clusters = csv.reader(file, delimiter=' ', quotechar='|')

    #     for c in clusters:
    #         v = g.add_vertex()
    #         g.vp[VP_NAME][v] = c[0]
    #         ntov[c[0]] = v

    with open(edge_file) as file:
        edges = csv.reader(file, delimiter=',', quotechar='|')

        for e in edges:
            c = e[3].strip()

            if(c == cluster_to_analyse.strip()):
                a = e[0]
                b = e[1]
                w = e[2]
                v1 = ntov.get(a)
                v2 = ntov.get(b)

                if not v1:
                    v1 = g.add_vertex()
                    g.vp[VP_NAME][v1] = a
                    ntov[a] = v1

                if not v2:
                    v2 = g.add_vertex()
                    g.vp[VP_NAME][v2] = b
                    ntov[b] = v2

                if v1 and v2:
                    edge = g.add_edge(v1, v2, add_missing=False)
                    g.ep[EP_WEIGHT][edge] = int(w)

def save_cluster_graph_to_file(folder):
    #gt.graph_draw(g, vertex_text=g.vp[VP_NAME], vertex_font_size=18, output_size=(10000, 10000), output= folder + "/graph.png")

    g.save(folder + "/graph.graphml")


if __name__ == "__main__":
    searchForCluster = sys.argv[1]
    outPath = sys.argv[2]
    edge_file = sys.argv[3]
    print("Build Cluster " + searchForCluster)
    print(edge_file)
    main(searchForCluster, outPath, edge_file)