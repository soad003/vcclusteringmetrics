#!/usr/bin/env python
import graph_tool.all as gt
import csv
import os
import matplotlib
import sys

# 1.) Ein ungerichteter Graph in dem Knoten Adressen und Kanten die Zahl der Transaktionen in denen zwei Adressen gemeinsam vorgekommen sind. Beispiel:

# a1 a2 3
# a2 a4 2
# a5 a6 1

# 2.) Die Zuordnung der Adressen zu Clustern (berechnet mittels multiple-input Heuristik). Beispiel:

# a1 c1
# a2 c1
# a4 c1
# a5 c2
# a6 c2


g = gt.Graph(directed=False)
VP_NAME = "name"
EP_WEIGHT = "weight"
GP_NTOV = "name_to_vertex"

g.edge_properties[EP_WEIGHT] = g.new_edge_property("int")
g.vertex_properties[VP_NAME] = g.new_vertex_property("string")
g.vertex_properties["pagerank"] = g.new_vertex_property("double")

g.vertex_properties["betwenness"] = g.new_vertex_property("double")
g.edge_properties["betwenness"] = g.new_edge_property("double")

g.vertex_properties["closeness"] = g.new_vertex_property("double")

g.graph_properties["central_point_dominance"] = g.new_graph_property("double")

g.graph_properties["eigenvalue"] = g.new_graph_property("double")
g.vertex_properties["eigenvector"] = g.new_vertex_property("double")

g.vertex_properties["katz"] = g.new_vertex_property("double")

g.vertex_properties["hits_x"] = g.new_vertex_property("double")
g.vertex_properties["hits_y"] = g.new_vertex_property("double")
g.graph_properties["hits_eigenvalue"] = g.new_graph_property("double")

g.vertex_properties["mincutparts"] = g.new_vertex_property("boolean")
g.graph_properties["mincutvalue"] = g.new_graph_property("double")

#g.graph_properties[GP_NTOV] = g.new_graph_property("python::object",{})
ntov = {}

#g.set_directed(False)

# def write_to_file(file, string):
#     with open(file, "w") as file:
#         file.write(str(string))

def add_to_vertex(g, prop, data):
    g.vp[prop] = data

def add_to_edges(g, prop, data):
    g.ep[prop] = data

def add_to_graph(g, prop, data):
    g.gp[prop] = data

def main(folder):
    load_cluster(folder)  

    # calc_metric_pagerank(g)
    # draw_pagerank(g, folder)

    # calc_betweenness(g)
    # draw_betweenness(g, folder)

    # calc_closeness(g)
    # draw_closeness(g, folder)


    calc_central_point_dominance(g)

    calc_eigenvalue(g)
    draw_eigenvalue(g, folder)

    # calc_katz(g)
    # draw_katz(g, folder)

    # calc_hits(g)
    # draw_hits(g, folder)

    calc_min_cut(g)
    draw_min_cut(g, folder)

    save_cluster_graph_to_file(folder)

def load_cluster(folder):
    load_cluster_graph_from_file(folder)

def save_cluster_graph_to_file(folder):
    g.save(folder + "/graph.graphml")

def load_cluster_graph_from_file(folder):
    return gt.load_graph(folder + "/graph.graphml")

def calc_metric_pagerank(g):
    #g = gt.GraphView(g, vfilt=gt.label_largest_component(g))
    pr = gt.pagerank(g, weight = g.edge_properties[EP_WEIGHT])
    add_to_vertex(g, "pagerank", pr)

def draw_pagerank(g, folder):
    pr = g.vertex_properties.pagerank
    gt.graph_draw(g, vertex_fill_color=pr,
               vertex_size=gt.prop_to_size(pr, mi=5, ma=15),
               vorder=pr, vcmap=matplotlib.cm.gist_heat,
               output=folder + "/page_rank.png")

def calc_betweenness(g):
    #g = gt.GraphView(g, vfilt=gt.label_largest_component(g))
    vp, ep = gt.betweenness(g, weight = g.edge_properties[EP_WEIGHT])

    add_to_vertex(g,"betwenness", vp)
    add_to_edges(g, "betwenness", ep)

def draw_betweenness(g, folder):
    vp = g.vp.betwenness
    ep = g.ep.betwenness
    gt.graph_draw(g, vertex_fill_color=vp,
               vertex_size=gt.prop_to_size(vp, mi=5, ma=15),
               edge_pen_width=gt.prop_to_size(ep, mi=0.5, ma=5),
               vcmap=matplotlib.cm.gist_heat,
               vorder=vp, output=folder + "/betweenness.png")

def calc_closeness(g):
    #g = gt.GraphView(g, vfilt=gt.label_largest_component(g))
    c = gt.closeness(g, weight = g.edge_properties[EP_WEIGHT])
    add_to_vertex(g,"closeness", c)

def draw_closeness(g, folder):
    c = g.vp.closeness
    gt.graph_draw(g, vertex_fill_color=c,
            vertex_size=gt.prop_to_size(c, mi=5, ma=15),
            vcmap=matplotlib.cm.gist_heat,
            vorder=c, output=folder + "/closeness.png")

def calc_central_point_dominance(g):
    #g = gt.GraphView(g, vfilt=gt.label_largest_component(g))
    vp, ep = gt.betweenness(g, weight = g.edge_properties[EP_WEIGHT])

    add_to_graph(g, "central_point_dominance", gt.central_point_dominance(g, vp))

def calc_eigenvalue(g):
    #g = gt.GraphView(g, vfilt=gt.label_largest_component(g))
    x, ee = gt.eigenvector(g, weight = g.edge_properties[EP_WEIGHT])
    add_to_graph(g, "eigenvalue", x)
    add_to_vertex(g, "eigenvector", ee)

def draw_eigenvalue(g, folder):
    ee = g.gp.eigenvalue
    x = g.vp.eigenvector
    gt.graph_draw(g, vertex_fill_color=x,
                   vertex_size=gt.prop_to_size(x, mi=5, ma=15),
                   vorder=x, vcmap=matplotlib.cm.gist_heat,
                   output=folder + "/eigenvector.png")

def calc_katz(g):
   # g = gt.GraphView(g, vfilt=gt.label_largest_component(g))
    x = gt.katz(g, weight = g.edge_properties[EP_WEIGHT])
    add_to_vertex(g, "katz", x)


def draw_katz(g, folder):
    x = g.vp.katz
    gt.graph_draw(g, vertex_fill_color=x,
               vertex_size=gt.prop_to_size(x, mi=5, ma=15),
               vorder=x, vcmap=matplotlib.cm.gist_heat,
               output=folder + "/katz.png")

def calc_hits(g):
    #g = gt.GraphView(g, vfilt=gt.label_largest_component(g))
    ee, x, y = gt.hits(g, weight = g.edge_properties[EP_WEIGHT])
    add_to_graph(g, "hits_eigenvalue", ee)
    add_to_vertex(g, "hits_x", x)
    add_to_vertex(g, "hits_y", y)

def draw_hits(g, folder):
    x = g.vp.hits_x
    y = g.vp.hits_y
    gt.graph_draw(g, vertex_fill_color=x,
                   vertex_size=gt.prop_to_size(x, mi=5, ma=15),
                   vorder=x, vcmap=matplotlib.cm.gist_heat,
                   output=folder + "/hits_x.png")
    gt.graph_draw(g, vertex_fill_color=y,
                   vertex_size=gt.prop_to_size(y, mi=5, ma=15),
                   vorder=y, vcmap=matplotlib.cm.gist_heat,
                   output=folder + "/hits_y.png")


def calc_min_cut(g):
    mc, part = gt.min_cut(g, weight = g.edge_properties[EP_WEIGHT])
    add_to_vertex(g, "mincutparts", part)
    add_to_graph(g, "mincutvalue", mc)

def draw_min_cut(g, folder):
    w = g.ep[EP_WEIGHT]
    part = g.vp.mincutparts
    gt.graph_draw(g, edge_pen_width=w, vertex_fill_color=part,
                    output=folder + "/min-cut.pdf")

if __name__ == "__main__":
    searchForCluster = sys.argv[1]
    main(searchForCluster)