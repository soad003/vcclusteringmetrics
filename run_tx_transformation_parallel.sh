#!/bin/sh -

mkdir ./data/temp

#sed -i '1d' filename

split -C 10m --numeric-suffixes ./data/tx_inputs.csv ./data/temp/tx_inputs_nr_

echo "parallel tx processing"

find . -name "tx_inputs_nr_*" |  parallel -I% --max-args 1 ./print_tx_edges_format.py -i % -o %_out -e /dev/null

echo "merge"

find . -name "tx_inputs_nr_*_out" | ./merge_tx_inputs.py

#find . -name "tx_inputs_nr_*_out" --exec "rm --f {}" \
