#!/bin/sh -

./print_cluster_between.sh $1 $2 | awk 'BEGIN {FS = ","}; {print $1}' | head -n $3 | parallel -I% --max-args 1 ./run_for_cluster.sh %
