#!/usr/bin/env python
import csv
import os
import sys

###############################################
#  
# Prints all addresses, belonging to one cluster
#
###############################################

def main(cluster):
    print_addresses_in_cluster(cluster)

def print_addresses_in_cluster(cluster):
    #addr_in_cluster = []
    with open("./data/address_cluster.csv") as cluster_in:  
        reader = csv.reader(cluster_in, delimiter=',', quotechar='|')
        next(reader, None)  # skip the headers
        for row in reader:
            if row[1].strip() == cluster.strip():
                addr = row[0].strip().lower()
                print(addr)

if __name__ == "__main__":
    main(sys.argv[1])