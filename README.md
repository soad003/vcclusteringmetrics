# Install notes

please install graph-tools as described here:

    https://git.skewed.de/count0/graph-tool/wikis/installation-instructions

Additionally, gnu parallel is used (`run_for_clusters.sh`). This should usually be available via a std. package manager. 

Finally, install the python packages needed via pip 

`pip install -r requirements.txt --user`

# Preprocessing

Tx data needs to be transformed before it can be used. To transform run:

`./print_tx_edges_format.py > ./data/tx_inputs.csv`

# Expected Files

./data/address_cluster.csv (format: [address],[cluster_id])

./data/cluster_stats.csv (format: [cluster_id],[nr_of_addresses])

./data/tx_inputs.csv (format: [tx_id], [yaml_list_of_inputs] ; only needed for preprocessing)

./data/weighted_edges.txt (format: [address1] [address2] [count]; calculated from tx_inputs.csv see preprocessing)

# Run

Run_for_cluster can be run for clusters already calculated, it skips all steps it has already done, instead of recomputing them. The file `done` in the folder signals that all calculations have been carried out already. 

*Single Cluster*
`./run_for_cluster.py [cluster_id]`

*Multiple Clusters*
`run_for_clusters.py [min_cluster_size] [max_cluster_size] [limit]` 

*Multiple arbitrary clusters*
`cat list_of_clusters.txt | parallel -I% --max-args 1 ./run_for_cluster.sh %`

# Metric calculation

Currently calculation of metrics cannot be tested because of the lack of transaction data. 



